import { CYPERLEARN_LOCALSTORAGE } from "../Constants";

export let localStorageServices = {
  setUser: (key, user) => {
    if (typeof localStorage !== 'undefined') {
      let dataJson = JSON.stringify(user);
      localStorage.setItem(key, dataJson);
    } else {
      console.error('localStorage is not available.');
    }
  },
  getUser: (user) => {
    if (typeof localStorage !== 'undefined') {
      let dataJson = localStorage.getItem(user);
      return JSON.parse(dataJson);
    } else {
      console.error('localStorage is not available.');
      return null; // hoặc giá trị mặc định tùy thuộc vào logic của bạn
    }
  },
  removeUser: () => {
    if (typeof localStorage !== 'undefined') {
      localStorage.removeItem(CYPERLEARN_LOCALSTORAGE);
    } else {
      console.error('localStorage is not available.');
    }
  },
};
