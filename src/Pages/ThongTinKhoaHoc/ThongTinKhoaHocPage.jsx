import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { Avatar, Input, Rate, message } from "antd";
import Lottie from "lottie-react";
import course from "./course.json";
import {
  LayThongTinKhoaHoc,
  layKhoaHocDangKy,
} from "../../redux/QuanLyKhoaHocSlice";
const ThongTinKhoaHocPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { maKhoaHoc } = useParams();
  const { infoCourse } = useSelector((state) => {
    return state.QuanLyKhoaHocSlice;
  });
  const { userLogin } = useSelector((state) => {
    return state.QuanLyNguoiDungSlice;
  });
  const params = {
    taiKhoan: userLogin?.taiKhoan,
    maKhoaHoc: infoCourse.maKhoaHoc,
  };
  useEffect(() => {
    dispatch(LayThongTinKhoaHoc(maKhoaHoc));
  }, []);
  const { danhMucKhoaHoc } = infoCourse;
  const handleDangKyKhoaHoc = () => {
    if (userLogin === null) {
      message.error(
        "Bạn chưa đăng nhập tài khoản. Hãy đăng nhập tài khoản rồi đăng ký khóa học nào!!!"
      );
      setTimeout(() => {
        navigate("/dang-nhap");
      }, 2000);
      return;
    }
    const fetch = async () => {
      await layKhoaHocDangKy(params)
        .then((res) => {
          console.log(res);
          message.success("Đăng ký thành công!!!");
        })
        .catch((err) => {
          message.error("Bạn đã đăng ký khóa học này rồi!!!");
          console.log(err);
        });
    };
    fetch();
  };
  const cssThongTinKhoaHocPage = `
    xl:flex xl:justify-center xl:items-center 
    lg:flex lg:justify-center lg:items-center 
    md:flex md:justify-center md:items-center 
    sm:flex sm:justify-center sm:items-center 
    min-[360px]:block
  `;
  return (
    <>
      <div className={cssThongTinKhoaHocPage}>
        <div className="xl:w-3/5 lg:w-3/5 md:w-3/5 sm:w-3/5 xl:ml-5 lg:ml-5 md:ml-5 sm:ml-5 text-center min-[360px]:container min-[360px]:w-full min-[360px]:pt-24">
          <h1 className="xl:text-4xl lg:text-4xl md:text-3xl sm:text-xl text-yellow-600 font-medium min-[360px]:text-2xl">
            CHỌN KHÓA HỌC THEO SỞ THÍCH CỦA CÁC BẠN. <br /> CHỨ KHÔNG HỌC CHO
            NGƯỜI KHÁC.
          </h1>
          <h1 className="animated-text xl:text-3xl lg:text-3xl md:text-2xl sm:text-lg min-[360px]:text-2xl text-green-600 font-medium">
            THÔNG TIN KHÓA HỌC - TIẾN LÊN VÀ KHÔNG CHẦN CHỪ
          </h1>
        </div>
        <div className="w-2/5 xl:h-screen lg:h-screen md:h-screen sm:h-screen lg:flex lg:items-center md:flex md:items-center sm:flex sm:items-center min-[360px]:w-full min-[360px]:container">
          <Lottie
            animationData={course}
            loop={true}
            width="100%"
            height="100%"
          />
        </div>
      </div>
      <div className="container lg:flex md:flex sm:block py-10">
        <div className="lg:hidden md:hidden sm:block sm:pb-10 min-[360px]:pb-10">
          <div className="border-2 p-3">
            <div className="">
              <img
                src="https://res.cloudinary.com/dmsxwwfb5/image/upload/v1595866967/full-stack-devlopment-min.png"
                width="100%"
                alt=""
                height={400}
              />
            </div>
            <div className="px-3">
              <h1 className="text-center text-xl sm:text-2xl text-gray-500 font-medium py-5">
                Miễn Phí Cho Các Viên Tại CyperSoft
              </h1>
              <button
                className="w-full py-2 my-3 rounded-2xl text-xl sm:text-2xl font-medium text-white bg-yellow-600 transition delay-200 border hover:-translate-y-2"
                onClick={() => {
                  handleDangKyKhoaHoc();
                }}
              >
                Đăng Ký
              </button>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Ghi Danh:{" "}
                    <span className="text-black font-medium">10 Học Viên</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i class="fas fa-user-graduate text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Thời Gian:{" "}
                    <span className="text-black font-medium">18 Giờ</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i className="far fa-clock text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Bài Học: <span className="text-black font-medium">10</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i class="fas fa-book text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Video: <span className="text-black font-medium">14</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i class="fas fa-video text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Trình Độ:{" "}
                    <span className="text-black font-medium">Beginner</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i className="fas fa-layer-group text-2xl text-yellow-600"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="lg:w-3/5 md:w-3/5">
          <h1 className="lg:text-4xl md:text-3xl sm:text-4xl min-[360px]:text-2xl text-yellow-600 font-medium border-b-4 border-black pb-5">
            THÔNG TIN KHÓA HỌC:
          </h1>
          <hr />
          <div className="border-b-4 border-black py-8">
            <h1 className="text-3xl font-medium">{infoCourse?.tenKhoaHoc}</h1>
            <div className="xl:flex lg:flex py-5">
              <div className="xl:w-1/3 lg:w-[30%] flex">
                <div className="w-[10%]">
                  <Avatar
                    src="https://i.pravatar.cc/?u=fake@pravatar.com"
                    className="lg:h-14 lg:w-14 md:h-16 md:w-16 sm:h-16 sm:w-16 min-[360px]:h-16 min-[360px]:w-16"
                  ></Avatar>
                </div>
                <div className="w-[90%] lg:pl-9 md:pl-9 sm:pl-5 min-[360px]:pl-9">
                  <p className="text-gray-500 md:text-xl sm:text-2xl min-[360px]:text-2xl">
                    Giảng Viên
                  </p>
                  <h1 className="color-changing-text xl:text-2xl lg:text-2xl md:text-2xl sm:text-3xl min-[360px]:text-2xl font-medium">
                    Trần Quang Sĩ
                  </h1>
                </div>
              </div>
              <div className="xl:w-1/3 lg:w-1/2 lg:pl-4 flex lg:mx-auto md:py-3 sm:py-3 min-[360px]:py-3">
                <div className="w-[10%] lg:text-right">
                  <i
                    class="fas fa-graduation-cap text-green-600"
                    style={{ fontSize: 50 }}
                  ></i>
                </div>
                <div className="w-[90%] lg:pl-9 md:pl-9 sm:pl-5 min-[360px]:pl-9">
                  <p className="text-gray-500 md:text-xl sm:text-2xl min-[360px]:text-2xl">
                    Lĩnh Vực
                  </p>
                  <h1 className="text-xl font-medium sm:text-2xl min-[360px]:text-2xl">
                    {danhMucKhoaHoc?.tenDanhMucKhoaHoc}
                  </h1>
                </div>
              </div>
              <div className="xl:w-1/3 lg:w-[20%] lg:text-right">
                <p>
                  <Rate allowHalf defaultValue={4.5} />
                  <span className="font-medium">(4.5)</span>
                </p>
                <h1 className="text-xl font-medium">
                  {infoCourse?.luotXem} đánh giá
                </h1>
              </div>
            </div>
            <h1 className="text-green-600 text-3xl font-medium">
              Mô Tả Khóa Học:
            </h1>
            <p className="text-xl text-gray-500 py-3 sm:text-2xl min-[360px]:text-2xl">
              {infoCourse?.moTa}
            </p>
          </div>
          <div className="py-5">
            <h1 className="text-3xl pb-5 font-medium sm:text-4xl min-[360px]:text-3xl">
              Những Gì Bạn Sẽ Học:
            </h1>
            <div class="flex">
              <div class="w-1/2">
                <ul>
                  <li className="pb-3">
                    <i className="fas fa-check text-yellow-600 mr-3"></i>
                    <span className="text-xl">
                      Xây dựng các ứng dụng web mạnh mẽ, nhanh chóng, thân thiện
                      với người dùng và phản ứng nhanh
                    </span>
                  </li>
                  <li className="pb-3">
                    <i className="fas fa-check text-yellow-600 mr-3"></i>
                    <span className="text-xl">
                      Đăng ký công việc được trả lương cao hoặc làm freelancer
                      trong một trong những lĩnh vực được yêu cầu nhiều nhất mà
                      bạn có thể tìm thấy trong web dev ngay bây giờ
                    </span>
                  </li>
                  <li className="pb-3">
                    <i className="fas fa-check text-yellow-600 mr-3"></i>
                    <span className="text-xl">
                      Cung cấp trải nghiệm người dùng tuyệt vời bằng cách tận
                      dụng sức mạnh của JavaScript một cách dễ dàng
                    </span>
                  </li>
                  <li className="pb-3">
                    <i className="fas fa-check text-yellow-600 mr-3"></i>
                    <span className="text-xl">
                      Tìm hiểu tất cả về React Hooks và React Components
                    </span>
                  </li>
                </ul>
              </div>
              <div className="w-1/2 pl-3">
                <ul>
                  <li className="pb-3">
                    <i className="fas fa-check text-yellow-600 mr-3"></i>
                    <span className="text-xl">
                      Thông thạo chuỗi công cụ hỗ trợ React, bao gồm cú pháp
                      Javascript NPM, Webpack, Babel và ES6 / ES2015
                    </span>
                  </li>
                  <li className="pb-3">
                    <i className="fas fa-check text-yellow-600 mr-3"></i>
                    <span className="text-xl">
                      Nhận ra sức mạnh của việc xây dựng các thành phần có thể
                      kết hợp
                    </span>
                  </li>
                  <li className="pb-3">
                    <i className="fas fa-check text-yellow-600 mr-3"></i>
                    <span className="text-xl">
                      Hãy là kỹ sư giải thích cách hoạt động của Redux cho mọi
                      người, bởi vì bạn biết rất rõ các nguyên tắc cơ bản
                    </span>
                  </li>
                  <li className="pb-3">
                    <i className="fas fa-check text-yellow-600 mr-3"></i>
                    <span className="text-xl">
                      Nắm vững các khái niệm cơ bản đằng sau việc cấu trúc các
                      ứng dụng Redux
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="lg:w-2/5 lg:block lg:px-5 md:block md:w-2/5 sm:hidden min-[360px]:hidden">
          <div className="border-2 p-3">
            <div className="">
              <img
                src="https://res.cloudinary.com/dmsxwwfb5/image/upload/v1595866967/full-stack-devlopment-min.png"
                width="100%"
                alt=""
                height={400}
              />
            </div>
            <div className="px-3">
              <h1 className="text-center text-xl text-gray-500 font-medium py-5">
                Miễn Phí Cho Các Viên Tại CyperSoft
              </h1>
              <button
                className="w-full py-2 my-3 rounded-2xl text-xl font-medium text-white bg-yellow-600 transition delay-200 border hover:-translate-y-2"
                onClick={() => {
                  handleDangKyKhoaHoc();
                }}
              >
                Đăng Ký
              </button>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Ghi Danh:{" "}
                    <span className="text-black font-medium">10 Học Viên</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i class="fas fa-user-graduate text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Thời Gian:{" "}
                    <span className="text-black font-medium">18 Giờ</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i className="far fa-clock text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Bài Học: <span className="text-black font-medium">10</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i class="fas fa-book text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Video: <span className="text-black font-medium">14</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i class="fas fa-video text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="flex py-5 border-b border-black">
                <div className="w-1/2">
                  <h1 className="text-gray-500 text-xl">
                    Trình Độ:{" "}
                    <span className="text-black font-medium">Beginner</span>
                  </h1>
                </div>
                <div className="w-1/2 text-right">
                  <i className="fas fa-layer-group text-2xl text-yellow-600"></i>
                </div>
              </div>
              <div className="py-5">
                <Input placeholder="Nhập Mã" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ThongTinKhoaHocPage;
