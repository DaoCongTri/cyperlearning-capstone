import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Form, Input, message } from "antd";
import Lottie from "lottie-react";
import animateLogin from "./login.json";
import {
  LayTaiKhoanDangNhap,
  setLogin,
} from "../../redux/QuanLyNguoiDungSlice";
import { CYPERLEARN_LOCALSTORAGE } from "../../Constants";
import { localStorageServices } from "../../Services/localStorageServices";
const DangNhap = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onFinish = (values) => {
    console.log("Success:", values);
    const fetch = async () => {
      await LayTaiKhoanDangNhap(values)
        .then((res) => {
          console.log(res);
          message.success("Đăng nhập thành công!!!");
          dispatch(setLogin(res.data));
          localStorageServices.setUser(CYPERLEARN_LOCALSTORAGE, res.data);
          setTimeout(() => {
            navigate("/");
          }, 2000);
        })
        .catch((err) => {
          message.error(err.response.data);
          console.log(err);
        });
    };
    fetch();
  };
  const cssDangNhap = `
    xl:flex xl:justify-center xl:items-center 
    lg:flex lg:justify-center lg:items-center 
    md:flex md:justify-center md:items-center 
    sm:flex sm:justify-center sm:items-center 
    min-[360px]:block
  `;
  const cssItemLeft = `
    xl:w-1/2 xl:h-screen 
    lg:w-1/2 lg:h-screen 
    md:w-[40%] md:h-screen md:flex md:items-center 
    sm:w-2/5 sm:flex sm:items-center sm:h-screen
    min-[360px]:w-full min-[360px]:h-screen min-[360px]:flex min-[360px]:items-center
  `;
  const cssItemRight = `
    xl:w-1/2 
    lg:w-1/2 
    md:w-[60%]
    sm:w-[60%]
    min-[360px]:container
  `;
  return (
    <div className={cssDangNhap}>
      <div className={cssItemLeft}>
        <Lottie
          animationData={animateLogin}
          className="justify-center"
          loop={true}
          width="100%"
          height="100%"
        />
      </div>
      <div className={cssItemRight}>
        <h1
          className="
        text-center pb-5 text-green-600 font-medium
        xl:text-4xl lg:text-3xl md:text-2xl sm:text-2xl 
        min-[360px]:text-2xl"
        >
          CHÀO MỪNG BẠN TRỞ LẠI
        </h1>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tài Khoản"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
            initialValue={"admin@"}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Mật Khẩu"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            initialValue={"Admin123"}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <button
              className="bg-yellow-600 py-2 xl:w-full lg:w-full md:w-full sm:w-full min-[360px]:px-5 rounded-xl text-2xl font-medium transition delay-300 hover:text-white"
              type="submit"
            >
              ĐĂNG NHẬP
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default DangNhap;
