import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Avatar } from "antd";
import { Dialog } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import Lottie from "lottie-react";
import bgAnimate from "./bg_animate.json";

import { useSelector } from "react-redux";
const LayoutTrangQuanTri = ({ contentPage }) => {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const navigate = useNavigate();
  const { userLogin } = useSelector((state) => {
    return state.QuanLyNguoiDungSlice;
  });
  return (
    <div>
      <header className="fixed inset-x-0 top-0 z-[999] bg-white shadow">
        <nav
          className="flex items-center justify-between p-6 lg:px-8"
          aria-label="Global"
        >
          <div className="flex lg:flex-1">
            <h1
              className="text-4xl text-yellow-600 font-bold cursor-pointer"
              onClick={() => {
                navigate("/");
              }}
            >
              TRANG CHỦ
            </h1>
          </div>
          <div className="flex lg:hidden">
            <button
              type="button"
              className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
              onClick={() => setMobileMenuOpen(true)}
            >
              <span className="sr-only">Open main menu</span>
              <Bars3Icon className="h-6 w-6" aria-hidden="true" />
            </button>
          </div>
          <div className="hidden lg:flex lg:gap-x-12">
            <div>
              <h1
                className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200"
                onClick={() => {
                  navigate("/trang-quan-tri/quan-ly-nguoi-dung");
                }}
              >
                QUẢN LÝ NGƯỜI DÙNG
              </h1>
            </div>
            <div>
              <h1
                className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200"
                onClick={() => {
                  navigate("/trang-quan-tri/quan-ly-khoa-hoc");
                }}
              >
                QUẢN LÝ KHÓA HỌC
              </h1>
            </div>
          </div>
          <div className="hidden lg:flex lg:flex-1 lg:justify-end">
            <h1 className="text-2xl font-medium">
              Xin Chào {userLogin.hoTen}
              <span className="pl-3">
                <Avatar
                  src="https://i.pravatar.cc/150?u=fake@pravatar.com"
                  style={{ width: 50, height: 50 }}
                  onClick={(e) => e.preventDefault()}
                />
              </span>
            </h1>
          </div>
        </nav>
        <Dialog
          as="div"
          className="lg:hidden"
          open={mobileMenuOpen}
          onClose={setMobileMenuOpen}
        >
          <div className="fixed inset-0 z-50" />
          <Dialog.Panel className="fixed inset-y-0 right-0 z-50 w-[300px] overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
            <div className="flex items-center justify-between">
              <a href="#" className="-m-1.5 p-1.5">
                <span className="sr-only">Your Company</span>
                <img
                  className="h-8 w-auto"
                  src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                  alt=""
                />
              </a>
              <button
                type="button"
                className="-m-2.5 rounded-md p-2.5 text-gray-700"
                onClick={() => setMobileMenuOpen(false)}
              >
                <span className="sr-only">Close menu</span>
                <XMarkIcon className="h-6 w-6" aria-hidden="true" />
              </button>
            </div>
            <div className="mt-6 flow-root">
              <div className="-my-6 divide-y divide-gray-500/10">
                <div className="space-y-2 py-6">
                  <div className="pt-5">
                    <h1
                      className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200"
                      onClick={() => {
                        navigate("/trang-quan-tri/quan-ly-nguoi-dung");
                      }}
                    >
                      QUẢN LÝ NGƯỜI DÙNG
                    </h1>
                  </div>
                  <div>
                    <h1
                      className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200"
                      onClick={() => {
                        navigate("/trang-quan-tri/quan-ly-khoa-hoc");
                      }}
                    >
                      QUẢN LÝ KHÓA HỌC
                    </h1>
                  </div>
                </div>
                <div className="py-6">
                  <h1 className="text-2xl font-medium">
                    Xin Chào {userLogin.hoTen}
                    <span className="pl-3">
                      <Avatar
                        src="https://i.pravatar.cc/150?u=fake@pravatar.com"
                        style={{ width: 50, height: 50 }}
                        onClick={(e) => e.preventDefault()}
                      />
                    </span>
                  </h1>
                </div>
              </div>
            </div>
          </Dialog.Panel>
        </Dialog>
      </header>
      <div
        className="
      pt-20 xl:flex xl:justify-center xl:items-center 
      lg:flex lg:justify-center lg:items-center 
      md:flex md:justify-center md:items-center
      sm:block min-[360px]:block"
      >
        <div className="xl:w-3/5 lg:w-3/5">
          <div>
            <h1 className="text-center py-5 text-4xl text-yellow-600 font-medium">
              TRANG QUẢN TRỊ
            </h1>
            <h1 className="animated-text xl:text-3xl lg:text-2xl md:text-xl text-center text-green-600">
              QUẢN LÝ NGƯỜI DÙNG - QUẢN LÝ KHÓA HỌC
            </h1>
          </div>
        </div>
        <div className="xl:w-2/5 lg:w-2/5 h-screen">
          <Lottie
            animationData={bgAnimate}
            className="justify-center"
            loop={true}
            width="100%"
            height="100%"
          />
        </div>
      </div>
      {contentPage}
    </div>
  );
};

export default LayoutTrangQuanTri;
