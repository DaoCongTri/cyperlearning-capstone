import React, { useEffect, useState } from "react";
import { Form, Input, Modal, Select, Upload, message } from "antd";
import { useSelector } from "react-redux";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import {
  LayDanhMucKhoaHoc,
  ThemKHUploadHinhAnh,
  listMaNhomKhoaHoc,
} from "../../../redux/QuanLyKhoaHocSlice";
const { TextArea } = Input;
const getBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
const FormThemKhoaHoc = () => {
  const [form] = Form.useForm();
  const [danhMuc, setDanhMuc] = useState([]);
  const { userLogin } = useSelector((state) => {
    return state.QuanLyNguoiDungSlice;
  });
  useEffect(() => {
    LayDanhMucKhoaHoc()
      .then((res) => {
        console.log(res.data);
        setDanhMuc(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const handleFininsh = async (values) => {
    console.log(values.hinhAnh.file);
    values.danhGia = Number(values.danhGia);
    values.luotXem = Number(values.luotXem);
    var formData = new FormData();
    formData.append("maKhoaHoc", values.maKhoaHoc);
    formData.append("tenKhoaHoc", values.tenKhoaHoc);
    formData.append("maDanhMucKhoaHoc", values.maDanhMucKhoaHoc);
    formData.append("ngayTao", values.ngayTao);
    formData.append("danhGia", values.danhGia);
    formData.append("luotXem", values.luotXem);
    formData.append("maNhom", values.maNhom);
    formData.append("taiKhoanNguoiTao", values.taiKhoanNguoiTao);
    formData.append("moTa", values.moTa);
    formData.append(
      "hinhAnh",
      values?.hinhAnh?.file?.originFileObj || values.hinhAnh
    );
    formData.append("biDanh", values.biDanh);
    const result = await ThemKHUploadHinhAnh(formData);
  };

  const [loading, setLoading] = useState(false);
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const handlePreview = async (file) => {
    console.log(file);
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    setPreviewOpen(true);
    setPreviewTitle(
      file.name || file.url.substring(file.url.lastIndexOf("/") + 1)
    );
  };

  const handleChange = async (info) => {
    if (info.file.status === "uploading") setLoading(true);
    if (info.file.status === "done") setLoading(false);
    if (info.file.status === "error") setLoading(false);
  };
  const handleCancel = () => setPreviewOpen(false);

  return (
    <>
      <Form
        form={form}
        name="themKhoaHoc"
        onFinish={handleFininsh}
        layout="inline"
        scrollToFirstError
        style={{
          maxWidth: 800,
        }}
      >
        {/* MÃ KHOÁ HỌC */}
        <Form.Item
          name="maKhoaHoc"
          className="w-[350px] pb-2"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập mã khóa học!!!",
            },
            {
              type: "string",
              pattern: /^\S+$/,
              message: "Mã khóa học không được có khoảng cách!!!",
            },
          ]}
        >
          <Input className="h-10" placeholder="Mã Khóa Học" />
        </Form.Item>

        {/* TÊN KHOÁ HỌC */}
        <Form.Item
          className="w-[350px] pb-2"
          name="tenKhoaHoc"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập tên khóa học!!!",
            },
          ]}
        >
          <Input className="h-10" placeholder="Tên Khóa Học" />
        </Form.Item>

        {/* BÍ DANH */}
        <Form.Item
          className="w-[350px] pb-2"
          name="biDanh"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập tên bí danh!!!",
            },
          ]}
        >
          <Input className="h-10" placeholder="Tên Bí Danh" />
        </Form.Item>

        {/* MÃ DANH MUC KHOÁ HỌC */}
        <Form.Item
          className="w-[350px] pb-2"
          name="maDanhMucKhoaHoc"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn danh mục khóa học!!!",
            },
          ]}
        >
          <Select size="large" placeholder="Danh Mục Khóa Học">
            {danhMuc.map((item, index) => {
              return (
                <Select.Option key={index} value={item.maDanhMuc}>
                  {item.tenDanhMuc}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>

        {/* NGÀY TẠO */}
        <Form.Item
          className="w-[350px] pb-2"
          name="ngayTao"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập ngày tạo khóa học!!!",
            },
            {
              type: "date",
              message: "Vui lòng đúng định dạng ngày tháng năm",
            },
          ]}
        >
          <Input className="h-10" placeholder="Ngày Tạo" />
        </Form.Item>

        {/* ĐÁNH GIÁ */}
        <Form.Item
          className="w-[350px] pb-2"
          name="danhGia"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập đánh giá của khóa học!!!",
            },
            {
              type: "string",
              pattern: /^[0-9]*$/,
              message: "Vui lòng nhập đánh giá bằng chữ số!!!",
            },
          ]}
        >
          <Input className="h-10" placeholder="Đánh Giá" />
        </Form.Item>

        {/* LƯỢT XEM */}
        <Form.Item
          className="w-[350px] pb-2"
          name="luotXem"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập lượt xem của khóa học!!!",
            },
            {
              type: "string",
              pattern: /^[0-9]*$/,
              message: "Vui lòng nhập lượt xem bằng chữ số!!!",
            },
          ]}
        >
          <Input className="h-10" placeholder="Lượt Xem" />
        </Form.Item>

        {/* MÃ NHÓM */}
        <Form.Item
          className="w-[350px] pb-2"
          name="maNhom"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn mã nhóm!!!",
            },
          ]}
        >
          <Select size="large" placeholder="Mã Nhóm">
            {listMaNhomKhoaHoc.map((item) => {
              return (
                <Select.Option key={item.id} value={item.maNhom}>
                  {item.maNhom}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>

        {/* TÀI KHOẢN NGƯỜI TẠO */}
        <Form.Item
          className="w-[350px] pb-2"
          name="taiKhoanNguoiTao"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập tài khoản người tạo!!!",
            },
            {
              type: "string",
              pattern: /^\S+$/,
              message: "Mã khóa học không được có khoảng cách!!!",
            },
          ]}
          initialValue={userLogin.taiKhoan}
        >
          <Input className="h-10" placeholder="Người Tạo" disabled />
        </Form.Item>

        {/* MÔ TẢ */}
        <Form.Item
          className="w-[350px]"
          name="moTa"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập mô tả của khóa học!!!",
            },
          ]}
        >
          <TextArea rows={5} placeholder="Mô Tả Khóa Học" />
        </Form.Item>

        {/* HÌNH ẢNH */}
        <Form.Item
          className="w-[350px]"
          name="hinhAnh"
          rules={[
            {
              required: true,
              message: "Vui lòng tải hình ảnh",
            },
          ]}
          valuePropName="file"
        >
          <Upload
            listType="picture-card"
            accept="image/png, image/jpeg"
            maxCount={1}
            customRequest={({ onSuccess }) => {
              setTimeout(() => {
                if (onSuccess) {
                  onSuccess("ok");
                }
              }, 0);
            }}
            onPreview={handlePreview}
            onChange={handleChange}
          >
            <div className="UPLOAD">
              {loading ? <LoadingOutlined /> : <PlusOutlined />}
              <div style={{ marginTop: 8 }}>Upload</div>
            </div>
          </Upload>
        </Form.Item>
        <Modal
          open={previewOpen}
          title={previewTitle}
          footer={null}
          onCancel={handleCancel}
        >
          <img
            alt="example"
            style={{
              width: "100%",
            }}
            src={previewImage}
          />
        </Modal>

        {/* BUTTON */}
        <div className="pt-5">
          <Form.Item>
            <button
              className="bg-yellow-600 px-3 py-2 rounded-xl text-2xl font-medium transition delay-300 hover:text-white"
              type="submit"
            >
              THÊM KHÓA HỌC
            </button>
          </Form.Item>
        </div>
      </Form>
    </>
  );
};

export default FormThemKhoaHoc;
