import React, { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { LayKhoaHocTheoDanhMuc } from "../../redux/QuanLyKhoaHocSlice";
import Lottie from "lottie-react";
import {
  DesktopOutlined,
  FieldTimeOutlined,
  CalendarOutlined,
  BarChartOutlined,
} from "@ant-design/icons";
import { Typography, Avatar } from "antd";
const { Paragraph, Text } = Typography;
import course from "./course.json";
import { useDispatch, useSelector } from "react-redux";
const DanhMucKhoaHocPage = () => {
  const navigate = useNavigate();
  const { maDanhMuc } = useParams();
  const dispatch = useDispatch();
  const { dsKHTheoDanhMuc } = useSelector((state) => {
    return state.QuanLyKhoaHocSlice;
  });
  useEffect(() => {
    dispatch(LayKhoaHocTheoDanhMuc(maDanhMuc));
  }, [maDanhMuc]);
  const products = dsKHTheoDanhMuc?.map((item, index) => {
    return {
      id: index + 1,
      maKhoaHoc: item.maKhoaHoc,
      biDanh: item.biDanh,
      tenKhoaHoc: item.tenKhoaHoc,
      moTa: item.moTa,
      luotXem: item.luotXem,
      hinhAnh:
        "https://www.altamira.ai/wp-content/uploads/2022/12/Full-Stack-DeveloperArtboard-2.png",
      ngayTao: item.ngayTao,
      nguoiTao: {
        hoTen: item.nguoiTao.hoTen,
      },
    };
  });
  const cssDanhMucKhoaHocPage = `
    xl:flex xl:justify-center xl:items-center 
    lg:flex lg:justify-center lg:items-center 
    md:flex md:justify-center md:items-center 
    sm:flex sm:justify-center sm:items-center 
    min-[360px]:block
  `;
  return (
    <div>
      <div className={cssDanhMucKhoaHocPage}>
        <div className="w-3/5 xl:ml-5 lg:ml-5 md:ml-5 sm:ml-5 text-center min-[360px]:w-full min-[360px]:pt-20">
          <h1 className="xl:text-4xl lg:text-4xl md:text-3xl sm:text-2xl text-yellow-600 font-medium min-[360px]:text-3xl">
            CHỌN KHÓA HỌC THEO SỞ THÍCH CỦA CÁC BẠN. <br /> CHỨ KHÔNG HỌC CHO
            NGƯỜI KHÁC.
          </h1>
          <h1 className="animated-text xl:text-3xl lg:text-3xl md:text-2xl sm:text-xl min-[360px]:text-2xl text-green-600 font-medium">
            Khóa Học Theo Danh Mục - {maDanhMuc}
          </h1>
        </div>
        <div className="w-2/5 h-screen pt-14 min-[360px]:w-full">
          <Lottie
            animationData={course}
            loop={true}
            width="100%"
            height="100%"
          />
        </div>
      </div>
      <div className="container">
        <button className="border-2 border-solid border-black rounded-3xl px-5 py-3 transition delay-200 hover:border-yellow-600">
          <DesktopOutlined
            style={{ fontSize: 48 }}
            className="text-yellow-600"
          ></DesktopOutlined>
          <span className="pl-2 pb-4 text-lg font-medium uppercase">
            {dsKHTheoDanhMuc?.slice(0, 1).map((item) => {
              return item.danhMucKhoaHoc.tenDanhMucKhoaHoc;
            })}
          </span>
        </button>
        <div className="course pt-14">
          <div className="bg-white">
            <div className="mx-auto max-w-2xl sm:px-6 sm:py-15 lg:py-10 lg:max-w-7xl">
              <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                {products.map((product) => (
                  <div
                    key={product.id}
                    className="group relative border shadow-md cursor-pointer transition delay-200 rounded-lg hover:-translate-y-2"
                    onClick={() => {
                      navigate(`/thongTinKhoaHoc/${product.maKhoaHoc}`);
                    }}
                  >
                    <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none lg:h-64">
                      <img
                        src={product.hinhAnh}
                        alt=""
                        className="w-full h-full object-cover object-center lg:h-full lg:w-full"
                      />
                    </div>
                    <div className="p-4">
                      <Text
                        className="text-yellow-600 py-3 text-2xl font-medium"
                        style={{
                          width: 80,
                        }}
                      >
                        {product.tenKhoaHoc}
                      </Text>
                      <Paragraph
                        className="text-gray-500 text-xl"
                        ellipsis={{
                          rows: 2,
                        }}
                      >
                        {product.moTa}
                      </Paragraph>
                      <div className="flex pb-5">
                        <div className="w-2/6 text-center">
                          <p>
                            <FieldTimeOutlined
                              style={{ fontSize: 25 }}
                              className="text-yellow-600"
                            />
                            <span className="pl-2 font-medium">8 Giờ</span>
                          </p>
                        </div>
                        <div className="w-2/6 text-center">
                          <p>
                            <CalendarOutlined
                              className="text-red-500"
                              style={{ fontSize: 25 }}
                            />
                            <span className="pl-2 font-medium">4 Tuần</span>
                          </p>
                        </div>
                        <div className="w-2/6 text-center">
                          <p>
                            <BarChartOutlined
                              className="text-blue-400"
                              style={{ fontSize: 25 }}
                            />
                            <span className="pl-2 font-medium">Tất Cả</span>
                          </p>
                        </div>
                      </div>
                      <hr />
                      <div className="flex justify-center items-center">
                        <div className="w-3/5 pt-4">
                          <div className="flex pb-5">
                            <div className="w-1/5">
                              <Avatar src="https://i.pravatar.cc/200?u=fake@pravatar.com "></Avatar>
                            </div>
                            <div className="w-4/5">
                              <p className="uppercase lg:text-sm md:text-base sm:text-base font-medium pl-1 min-[360px]:text-lg">
                                {product.nguoiTao.hoTen}
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="w-2/5 text-right">
                          <p className="line-through text-xs text-center">
                            800.000đ
                          </p>
                          <h1 className="color-changing-text xl:text-2xl lg:text-2xl md:text-2xl sm:text-xl min-[360px]:text-2xl text-center">
                            Miễn Phí
                          </h1>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DanhMucKhoaHocPage;
