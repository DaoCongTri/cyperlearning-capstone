import React from "react";
import Lottie from "lottie-react";
import bg1 from "./bg_animate1.json";
import bg2 from "./bg_animate2.json";
import bg3 from "./bg_animate3.json";
import bg4 from "./bg-animate4.json";
const ThongTinPage = () => {
  return (
    <>
      <div className="sliderAbout">
        <h1 className="text-center text-green-600 lg:text-3xl font-medium">
          CYPERLEARNING HỌC LÀ VUI
        </h1>
        <h1 className="text-center lg:text-4xl text-yellow-600 font-medium py-5">
          Cùng Nhau Khám Phá Những Điều Mới Mẻ
        </h1>
        <h1 className="text-center lg:text-3xl font-medium text-white">
          Học Đi Đôi Với Hành
        </h1>
      </div>
      <div className="p-12">
        <div
          className="
            xl:flex lg:flex md:flex 
            justify-center items-center 
            sm:block min-[360px]:block"
        >
          <div className="xl:hidden lg:hidden md:hidden sm:block min-[360px]:block">
            <h1 className="text-2xl text-green-600 font-medium">
              CYPERLEARNING HỌC GÌ ?
            </h1>
            <h1 className="text-4xl min-[360px]:text-3xl py-3 font-medium">
              Nơi Hội Tụ Kiến Thức
            </h1>
            <p className="text-2xl min-[360px]:text-xl text-gray-500">
              <span className="text-5xl text-red-500">Đ</span>ây là nền tảng
              giảng dạy và học tập trực tuyến được xây dựng để hỗ trợ phát triển
              giáo dục trong thời đại công nghiệp 4.0, được xây dựng dựa trên cơ
              sở quan sát toàn bộ các nhu cầu cần thiết của một lớp học offline.
              Từ đó đáp ứng và đảm bảo cung cấp các công cụ toàn diện, dễ sử
              dụng cho giáo viên và học sinh, giúp tạo nên một lớp học trực
              tuyến thú vị và hấp dẫn.
            </p>
          </div>
          <div className="xl:w-1/2 xl:block lg:block lg:w-1/2 md:block md:w-1/2 sm:hidden min-[360px]:hidden">
            <h1 className="text-2xl text-green-600 font-medium">
              CYPERLEARNING HỌC GÌ ?
            </h1>
            <h1 className="text-4xl py-3 font-medium">Nơi Hội Tụ Kiến Thức</h1>
            <p className="text-2xl text-gray-500">
              <span className="text-5xl text-red-500">Đ</span>ây là nền tảng
              giảng dạy và học tập trực tuyến được xây dựng để hỗ trợ phát triển
              giáo dục trong thời đại công nghiệp 4.0, được xây dựng dựa trên cơ
              sở quan sát toàn bộ các nhu cầu cần thiết của một lớp học offline.
              Từ đó đáp ứng và đảm bảo cung cấp các công cụ toàn diện, dễ sử
              dụng cho giáo viên và học sinh, giúp tạo nên một lớp học trực
              tuyến thú vị và hấp dẫn.
            </p>
          </div>
          <div className="xl:w-1/2 lg:w-1/2 md:w-1/2 sm:block min-[360px]:block">
            <Lottie
              animationData={bg1}
              className="justify-center"
              loop={true}
              width="100%"
              height={500}
            />
          </div>
        </div>
        <div
          className="
        xl:flex lg:flex md:flex 
        justify-center items-center 
        sm:block min-[360px]:block min-[360px]:pb-10"
        >
          <div className="xl:hidden lg:hidden md:hidden sm:block min-[360px]:block">
            <h1 className="text-2xl text-green-600 font-medium">
              CHƯƠNG TRÌNH HỌC CYPERLEARNING
            </h1>
            <h1 className="text-4xl min-[360px]:text-3xl py-3 font-medium">
              Hệ Thống Học Hàng Đầu
            </h1>
            <p className="text-2xl min-[360px]:text-xl text-gray-500">
              <span className="text-5xl text-red-500">G</span>iảng viên đều là
              các chuyên viên thiết kế đồ họa và giảng viên của các trường đại
              học danh tiếng trong thành phố: Đại học CNTT, KHTN, Bách
              Khoa,…Trang thiết bị phục vụ học tập đầy đủ, phòng học máy lạnh,
              màn hình LCD, máy cấu hình mạnh, mỗi học viên thực hành trên một
              máy riêng.100% các buổi học đều là thực hành trên máy tính. Đào
              tạo với tiêu chí: “học để làm được việc”, mang lại cho học viên
              những kiến thức hữu ích nhất, sát với thực tế nhất.
            </p>
          </div>
          <div className="xl:w-1/2 xl:block lg:block lg:w-1/2 md:block md:w-1/2 sm:block min-[360px]:block">
            <Lottie
              animationData={bg2}
              className="justify-center"
              loop={true}
              width="100%"
              height={500}
            />
          </div>
          <div className="xl:w-1/2 xl:block lg:w-1/2 lg:block md:w-1/2 md:block sm:hidden min-[360px]:hidden">
            <h1 className="text-2xl text-green-600 font-medium">
              CHƯƠNG TRÌNH HỌC CYPERLEARNING
            </h1>
            <h1 className="text-4xl py-3 font-medium">Hệ Thống Học Hàng Đầu</h1>
            <p className="text-2xl text-gray-500">
              <span className="text-5xl text-red-500">G</span>iảng viên đều là
              các chuyên viên thiết kế đồ họa và giảng viên của các trường đại
              học danh tiếng trong thành phố: Đại học CNTT, KHTN, Bách
              Khoa,…Trang thiết bị phục vụ học tập đầy đủ, phòng học máy lạnh,
              màn hình LCD, máy cấu hình mạnh, mỗi học viên thực hành trên một
              máy riêng.100% các buổi học đều là thực hành trên máy tính. Đào
              tạo với tiêu chí: “học để làm được việc”, mang lại cho học viên
              những kiến thức hữu ích nhất, sát với thực tế nhất.
            </p>
          </div>
        </div>
        <div
          className="
            xl:flex lg:flex md:flex 
            justify-center items-center 
            sm:block min-[360px]:block"
        >
          <div className="xl:hidden lg:hidden md:hidden sm:block min-[360px]:block">
            <h1 className="text-2xl text-green-600 font-medium">
              TẦM NHÌN CYPERLEARNING
            </h1>
            <h1 className="text-4xl min-[360px]:text-3xl py-3 font-medium">
              Đào Tạo Lập Trình Chuyên Sâu
            </h1>
            <p className="text-2xl min-[360px]:text-xl text-gray-500">
              <span className="text-5xl text-red-500">T</span>rở thành hệ thống
              đào tạo lập trình chuyên sâu theo nghề hàng đầu khu vực, cung cấp
              nhân lực có tay nghề cao, chuyên môn sâu cho sự phát triển công
              nghiệp phần mềm trong thời đại công nghệ số hiện nay, góp phần
              giúp sự phát triển của xã hội, đưa Việt Nam thành cường quốc về
              phát triển phần mềm.giúp người học phát triển cả tư duy, phân
              tích, chuyên sâu nghề nghiệp, học tập suốt đời, sẵn sàng đáp ứng
              mọi nhu cầu của doanh nghiệp.
            </p>
          </div>
          <div className="xl:w-1/2 xl:block lg:block lg:w-1/2 md:block md:w-1/2 sm:hidden min-[360px]:hidden">
            <h1 className="text-2xl text-green-600 font-medium">
              TẦM NHÌN CYPERLEARNING
            </h1>
            <h1 className="text-4xl py-3 font-medium">
              Đào Tạo Lập Trình Chuyên Sâu
            </h1>
            <p className="text-2xl text-gray-500">
              <span className="text-5xl text-red-500">T</span>rở thành hệ thống
              đào tạo lập trình chuyên sâu theo nghề hàng đầu khu vực, cung cấp
              nhân lực có tay nghề cao, chuyên môn sâu cho sự phát triển công
              nghiệp phần mềm trong thời đại công nghệ số hiện nay, góp phần
              giúp sự phát triển của xã hội, đưa Việt Nam thành cường quốc về
              phát triển phần mềm.giúp người học phát triển cả tư duy, phân
              tích, chuyên sâu nghề nghiệp, học tập suốt đời, sẵn sàng đáp ứng
              mọi nhu cầu của doanh nghiệp.
            </p>
          </div>
          <div className="xl:w-1/2 lg:w-1/2 md:w-1/2 sm:block min-[360px]:block">
            <Lottie
              animationData={bg3}
              className="justify-center"
              loop={true}
              width="100%"
              height={500}
            />
          </div>
        </div>
        <div
          className="
        xl:flex lg:flex md:flex 
        justify-center items-center 
        sm:block min-[360px]:block min-[360px]:pb-10"
        >
          <div className="xl:hidden lg:hidden md:hidden sm:block min-[360px]:block">
            <h1 className="text-2xl text-green-600 font-medium">
              SỨ MỆNH CYPERLEARNING
            </h1>
            <h1 className="text-4xl min-[360px]:text-3xl py-3 font-medium">
              Phương Pháp Đào Tạo Hiện Đại
            </h1>
            <p className="text-2xl min-[360px]:text-xl text-gray-500">
              <span className="text-5xl text-red-500">S</span>ử dụng các phương
              pháp đào tạo tiên tiến và hiện đại trên nền tảng công nghệ giáo
              dục, kết hợp phương pháp truyền thống, phương pháp trực tuyến, lớp
              học đảo ngược và học tập dựa trên dự án thực tế, phối hợp giữa đội
              ngũ training nhiều kinh nghiệm và yêu cầu từ các công ty, doanh
              nghiệp. Qua đó, V learning giúp người học phát triển cả tư duy,
              phân tích, chuyên sâu nghề nghiệp, học tập suốt đời, sẵn sàng đáp
              ứng mọi nhu cầu của doanh nghiệp.
            </p>
          </div>
          <div className="xl:w-1/2 xl:block lg:block lg:w-1/2 md:block md:w-1/2 sm:block min-[360px]:block">
            <Lottie
              animationData={bg4}
              className="justify-center"
              loop={true}
              width="100%"
              height={500}
            />
          </div>
          <div className="xl:w-1/2 xl:block lg:w-1/2 lg:block md:w-1/2 md:block sm:hidden min-[360px]:hidden">
            <h1 className="text-2xl text-green-600 font-medium">
              SỨ MỆNH CYPERLEARNING
            </h1>
            <h1 className="text-4xl py-3 font-medium">
              Phương Pháp Đào Tạo Hiện Đại
            </h1>
            <p className="text-2xl text-gray-500">
              <span className="text-5xl text-red-500">S</span>ử dụng các phương
              pháp đào tạo tiên tiến và hiện đại trên nền tảng công nghệ giáo
              dục, kết hợp phương pháp truyền thống, phương pháp trực tuyến, lớp
              học đảo ngược và học tập dựa trên dự án thực tế, phối hợp giữa đội
              ngũ training nhiều kinh nghiệm và yêu cầu từ các công ty, doanh
              nghiệp. Qua đó, V learning giúp người học phát triển cả tư duy,
              phân tích, chuyên sâu nghề nghiệp, học tập suốt đời, sẵn sàng đáp
              ứng mọi nhu cầu của doanh nghiệp.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default ThongTinPage;
