import "./Services/config";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/Home/HomePage";
import Layout from "./Layout/Layout";
import DanhMucKhoaHocPage from "./Pages/DanhMucKhoaHoc/DanhMucKhoaHocPage";
import KhoaHocPage from "./Pages/KhoaHoc/KhoaHocPage";
import ThongTinKhoaHocPage from "./Pages/ThongTinKhoaHoc/ThongTinKhoaHocPage";
import DangNhap from "./Pages/DangNhap_DangKy/DangNhap";
import DangKy from "./Pages/DangNhap_DangKy/DangKy";
import TrangQuanTri from "./Pages/TrangQuanTri/LayoutTrangQuanTri";
import TrangThongTinNguoiDung from "./Pages/TrangThongTinNguoiDung/TrangThongTinNguoiDung";
import QuanLyNguoiDungPage from "./Pages/TrangQuanTri/QuanLyNguoiDung/QuanLyNguoiDungPage";
import QuanLyKhoaHocPage from "./Pages/TrangQuanTri/QuanLyKhoaHoc/QuanLyKhoaHocPage";
import ThongTinPage from "./Pages/ThongTin/ThongTinPage";
import Spinner from "./Components/Spinner/Spinner";
import LayoutTrangQuanTri from "./Pages/TrangQuanTri/LayoutTrangQuanTri";
function App() {
  return (
    <>
      <Spinner />
      <BrowserRouter>
        <Routes>
          {/* User Layout */}
          <Route element={<Layout />}>
            <Route path="/" element={<HomePage />} />
            <Route
              path="danhMucKhoaHoc/:maDanhMuc"
              element={<DanhMucKhoaHocPage />}
            />
            <Route path="khoaHoc" element={<KhoaHocPage />} />
            <Route path="thongTin" element={<ThongTinPage />} />
            <Route
              path="thongTinKhoaHoc/:maKhoaHoc"
              element={<ThongTinKhoaHocPage />}
            />
            <Route path="dang-nhap" element={<DangNhap />} />
            <Route path="dang-ky" element={<DangKy />} />
            <Route
              path="thong-tin-nguoi-dung"
              element={<TrangThongTinNguoiDung />}
            />
          </Route>
          {/* Admin Layout */}
          <Route path="/trang-quan-tri" element={<LayoutTrangQuanTri />} />
          <Route
            path="/trang-quan-tri/quan-ly-nguoi-dung"
            element={
              <LayoutTrangQuanTri contentPage={<QuanLyNguoiDungPage />} />
            }
          />
          <Route
            path="/trang-quan-tri/quan-ly-khoa-hoc"
            element={<LayoutTrangQuanTri contentPage={<QuanLyKhoaHocPage />} />}
          />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
