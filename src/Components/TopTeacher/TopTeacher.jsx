import { Avatar, Col, Rate, Row } from "antd";
import React from "react";
import Slider from "react-slick";

export default function TopTeacher() {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className="container py-14">
      <h1 className="text-yellow-600 text-3xl font-medium pb-14">
        GIẢNG VIÊN HÀNG ĐẦU
      </h1>
      <Slider {...settings}>
        <div>
          <Row gutter={[20, 20]}>
            <Col xl={6} xs={24} lg={6} md={12} sm={12}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Dennis Ritchie</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Ngôn Ngữ C
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
            <Col xl={6} xs={24} lg={6} md={12} sm={12}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Bjarne Stroustrup</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Ngôn Ngữ C++
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
            <Col xl={6} xs={24} lg={6} md={12} sm={12}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">James Gosling</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Ngôn Ngữ Java
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
            <Col xl={6} xs={24} lg={6} md={12} sm={12}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Linus Torvalds</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Hệ Điều Hành Linux
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
          </Row>
        </div>
        <div>
          <Row>
            <Col xl={6} xs={24} lg={6} md={12} sm={12}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Anders Hejlsberg</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Ngôn Ngữ C#
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
            <Col xl={6} xs={24} lg={6} md={12} sm={12}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Tim Berners – Lee</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Ngôn Ngữ HTML
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
            <Col xl={6} xs={24} lg={6} md={12} sm={12}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Brian Kernighan</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Ngôn Ngữ AWK/AMPL
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
            <Col xl={6} xs={24} lg={6} md={12} sm={12}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Ken Thompson</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Ngôn Ngữ B/Go
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
          </Row>
        </div>
        <div>
          <Row>
            <Col xl={6} xs={24}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Guido van Rossum</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Lập Trình Ngôn Ngữ Python
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
            <Col xl={6} xs={24}>
              <div className="text-center cursor-pointer">
                <Avatar
                  src="https://i.pravatar.cc/300"
                  style={{ width: 100, height: 100 }}
                />
                <p className="text-2xl font-medium">Donald Knuth</p>
                <p className="text-xl font-medium">
                  Chuyên Gia Lĩnh Vực <br /> Phân Tích Thuật Toán
                </p>
                <Rate allowHalf defaultValue={5} />
              </div>
            </Col>
          </Row>
        </div>
      </Slider>
    </div>
  );
}
