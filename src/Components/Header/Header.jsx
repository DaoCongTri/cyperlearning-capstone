import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { localStorageServices } from "../../Services/localStorageServices";
import { Dialog } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import { Avatar, Input } from "antd";
import {
  LayDanhMucKhoaHoc,
  LayDanhSachKhoaHocTheoTenUser,
} from "../../redux/QuanLyKhoaHocSlice";
import { useDispatch, useSelector } from "react-redux";
import { UserOutlined } from "@ant-design/icons";
import { setLogin } from "../../redux/QuanLyNguoiDungSlice";
const Header = () => {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const [danhMuc, setDanhMuc] = useState([]);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  useEffect(() => {
    LayDanhMucKhoaHoc()
      .then((res) => {
        setDanhMuc(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const { userLogin } = useSelector((state) => {
    return state.QuanLyNguoiDungSlice;
  });
  const handleLogOut = () => {
    localStorageServices.removeUser();
    dispatch(setLogin(null));
    navigate("/");
  };
  const handleTimKiem = (event) => {
    setTimeout(() => {
      navigate("/khoaHoc");
    }, 1000);
    if (event.target.value === null) return;
    const fetch = () => {
      dispatch(LayDanhSachKhoaHocTheoTenUser(1, event.target.value));
    };
    fetch();
  };
  const checkUserLogin = () => {
    if (userLogin !== null) {
      return (
        <>
          <div className="dropdown" style={{ float: "right" }}>
            <Avatar
              src="https://i.pravatar.cc/?u=fake@pravatar.com"
              style={{ height: 50, width: 50 }}
            ></Avatar>
            <div className="dropdown-user">
              <h1 className="text-2xl text-yellow-600 font-medium pb-3">
                {userLogin.hoTen}{" "}
                {userLogin.maLoaiNguoiDung === "GV" ? (
                  <span>- Admin</span>
                ) : (
                  <></>
                )}
              </h1>
              <h1
                className="pb-3 text-2xl font-medium transition delay-200 hover:text-green-600"
                onClick={() => {
                  navigate("/thong-tin-nguoi-dung");
                }}
              >
                Thông Tin Cá Nhân
              </h1>
              {userLogin.maLoaiNguoiDung === "GV" ? (
                <h1
                  className="pb-3 text-2xl font-medium transition delay-200 hover:text-green-600"
                  onClick={() => {
                    navigate("/trang-quan-tri");
                  }}
                >
                  Trang Quản Trị
                </h1>
              ) : (
                <></>
              )}
              <button
                className="bg-yellow-600 w-full py-2 text-2xl font-medium transition delay-200 hover:text-white"
                onClick={handleLogOut}
              >
                Đăng Xuất
              </button>
            </div>
          </div>
        </>
      );
    }
    return (
      <>
        <div className="dropdown" style={{ float: "right" }}>
          <Avatar size={50} icon={<UserOutlined />} />
          <div className="dropdown-user">
            <button
              className="bg-yellow-600 w-full py-2 mb-3 text-xl font-medium transition delay-200 hover:text-white"
              onClick={() => {
                navigate("/dang-nhap");
              }}
            >
              Đăng Nhập
            </button>
            <button
              className="bg-yellow-600 w-full py-2 text-xl font-medium transition delay-200 hover:text-white"
              onClick={() => {
                navigate("/dang-ky");
              }}
            >
              Đăng Ký
            </button>
          </div>
        </div>
      </>
    );
  };
  return (
    <div className="xl:py-12 min-[360px]:py-2">
      <header className="fixed inset-x-0 top-0 z-[999] bg-white shadow">
        <nav
          className="flex items-center justify-between p-6 lg:px-8"
          aria-label="Global"
        >
          <div className="flex lg:flex-1">
            <h1
              className="text-4xl text-yellow-600 font-bold cursor-pointer"
              onClick={() => {
                navigate("/");
              }}
            >
              CYPERLEARNING
            </h1>
          </div>
          <div className="flex lg:hidden">
            <button
              type="button"
              className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
              onClick={() => setMobileMenuOpen(true)}
            >
              <span className="sr-only">Open main menu</span>
              <Bars3Icon className="h-6 w-6" aria-hidden="true" />
            </button>
          </div>
          <div className="hidden lg:flex lg:gap-x-12">
            <div className="dropdown-menu">
              <span className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200">
                DANH MỤC
              </span>
              <div className="dropdown-content">
                {danhMuc.map((item) => {
                  return (
                    <h1
                      className="uppercase py-3 cursor-pointer font-medium transition delay-200 hover:text-green-600"
                      key={item.maDanhMuc}
                      onClick={() => {
                        navigate(`/danhMucKhoaHoc/${item.maDanhMuc}`);
                      }}
                    >
                      {item.tenDanhMuc}
                    </h1>
                  );
                })}
              </div>
            </div>
            <div>
              <h1
                className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200"
                onClick={() => {
                  navigate("/khoaHoc");
                }}
              >
                KHÓA HỌC
              </h1>
            </div>
            <div>
              <h1
                className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200"
                onClick={() => {
                  navigate("/thongTin");
                }}
              >
                THÔNG TIN
              </h1>
            </div>
            <div>
              <Input placeholder="Tìm Kiếm" onChange={handleTimKiem} />
            </div>
          </div>
          <div className="hidden lg:flex lg:flex-1 lg:justify-end">
            {checkUserLogin()}
          </div>
        </nav>
        <Dialog
          as="div"
          className="lg:hidden"
          open={mobileMenuOpen}
          onClose={setMobileMenuOpen}
        >
          <div className="fixed inset-0 z-50" />
          <Dialog.Panel className="fixed inset-y-0 right-0 z-50 w-[300px] overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
            <div className="flex items-center justify-between">
              <a href="#" className="-m-1.5 p-1.5">
                <span className="sr-only">Your Company</span>
                <img
                  className="h-8 w-auto"
                  src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                  alt=""
                />
              </a>
              <button
                type="button"
                className="-m-2.5 rounded-md p-2.5 text-gray-700"
                onClick={() => setMobileMenuOpen(false)}
              >
                <span className="sr-only">Close menu</span>
                <XMarkIcon className="h-6 w-6" aria-hidden="true" />
              </button>
            </div>
            <div className="mt-6 flow-root">
              <div className="-my-6 divide-y divide-gray-500/10">
                <div className="space-y-2 py-6">
                  <div className="dropdown-menu pt-5">
                    <span className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200">
                      DANH MỤC
                    </span>
                    <div className="dropdown-content">
                      {danhMuc.map((item) => {
                        return (
                          <h1
                            className="uppercase py-3 cursor-pointer transition delay-200 hover:text-green-600"
                            key={item.maDanhMuc}
                            onClick={() => {
                              navigate(`/danhMucKhoaHoc/${item.maDanhMuc}`);
                            }}
                          >
                            {item.tenDanhMuc}
                          </h1>
                        );
                      })}
                    </div>
                  </div>
                  <div>
                    <h1
                      className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200"
                      onClick={() => {
                        navigate("/khoaHoc");
                      }}
                    >
                      KHÓA HỌC
                    </h1>
                  </div>
                  <div>
                    <h1
                      className="text-xl font-medium cursor-pointer hover:text-green-600 transition-all delay-200"
                      onClick={() => {
                        navigate("/thongTin");
                      }}
                    >
                      THÔNG TIN
                    </h1>
                  </div>
                  <div>
                    <Input placeholder="Tìm Kiếm" onChange={handleTimKiem} />
                  </div>
                </div>
                <div className="py-6">{checkUserLogin()}</div>
              </div>
            </div>
          </Dialog.Panel>
        </Dialog>
      </header>
    </div>
  );
};

export default Header;
