import React from "react";
import Lottie from "lottie-react";
import student1 from "./student1.json";
import schedule from "./schedule.json";
import timer from "./timer.json";
import teacher from "./teacher.json";
import CountUp from "react-countup";
const Counter = () => {
  return (
    <div className="py-14 bg-slate-200">
      <div className="container grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
        <div className="">
          <div className="text-center">
            <Lottie
              animationData={student1}
              className="justify-center"
              loop={true}
              style={{ height: 150, width: 150, margin: "auto" }}
            />
            <h1 className="text-green-600 text-6xl font-bold py-4">
              <CountUp end={9000} duration={10} />
            </h1>
            <p className="text-2xl font-medium">Học Viên</p>
          </div>
        </div>
        <div className="">
          <div className="text-center">
            <Lottie
              animationData={schedule}
              className="justify-center"
              loop={true}
              style={{ height: 150, width: 150, margin: "auto" }}
            />
            <h1 className="text-green-600 text-6xl font-bold py-4">
              <CountUp end={1000} duration={10} />
            </h1>
            <p className="text-2xl font-medium">Khóa Học</p>
          </div>
        </div>
        <div className="">
          <div className="text-center">
            <Lottie
              animationData={timer}
              className="justify-center"
              loop={true}
              style={{ height: 150, width: 150, margin: "auto" }}
            />
            <h1 className="text-green-600 text-6xl font-bold py-4">
              <CountUp end={33200} duration={10} />
            </h1>
            <p className="text-2xl font-medium">Giờ Học</p>
          </div>
        </div>
        <div className="">
          <div className="text-center">
            <Lottie
              animationData={teacher}
              className="justify-center"
              loop={true}
              style={{ height: 150, width: 150, margin: "auto" }}
            />
            <h1 className="text-green-600 text-6xl font-bold py-4">
              <CountUp end={400} duration={10} />
            </h1>
            <p className="text-2xl font-medium">Giảng Viên</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Counter;
