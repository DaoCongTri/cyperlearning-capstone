import React from "react";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import animate_loading from "./animation_loading.json";
function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);
  return isLoading ? (
    <div className="h-screen fixed top-0 left-0 z-20 flex justify-center items-center">
      <Lottie
        animationData={animate_loading}
        className="w-screen"
        loop={true}
        width="100%"
        height="100%"
      />
    </div>
  ) : (
    <></>
  );
}

export default Spinner;
