import React from "react";
import ReactDOM from "react-dom/client";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import QuanLyNguoiDungSlice from "./redux/QuanLyNguoiDungSlice";
import QuanLyKhoaHocSlice from "./redux/QuanLyKhoaHocSlice.jsx";
import spinnerSlice from "./redux/spinnerSlice";
import App from "./App.jsx";
import "./index.css";
export let store = configureStore({
  reducer: {
    QuanLyNguoiDungSlice,
    QuanLyKhoaHocSlice,
    spinnerSlice,
  },
});
ReactDOM.createRoot(document.getElementById("root")).render(
  <Provider store={store}>
    <App />
  </Provider>
);
